
public class start {
	
	public String errorMessage;
 
	
	public static void main(String[] args) {
		
		start x = new start();
		
		if (x.EmailValidation("andrea@testcom")) {
			System.out.println("OK" );
		}else{
			System.out.println("Error: " + x.errorMessage);
		}
	}	
	
	
	public boolean EmailValidation (String email){
			
		
		   if (email.indexOf("@") == -1) {
			    this.errorMessage = "@ is missing";
		    	return false;
		    } 
		    if (email.indexOf(".") == -1){
		    	this.errorMessage = ". is missing";
		    	return false;
		    } 
		    
		    if (email.indexOf(" ") > 0){
		    	this.errorMessage = "remove the space ";
		    	return false;
		    }
		    
		    return true;
	}
	
	
	

}
